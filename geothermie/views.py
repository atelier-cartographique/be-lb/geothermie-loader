from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.views.decorators.cache import cache_page
from django.views.decorators.http import require_http_methods
from django.core.serializers import serialize
from django.contrib.gis.geos import GEOSGeometry
from math import pow, floor, sqrt
import json
from geothermie.models import (
    BASE_ROCK_DEPTH,
    DEFAULT_POINT,
    get_point_thickness,
    get_point_quat_thickness,
    get_mean_thickness,
    get_point_saturation_rate,
    get_point_phreatic_head,
    get_point_piezo,
    get_point_natura,
    get_point_water,
    get_point_soil,
    get_point_region,
    get_point_metro_nord,
    MapSelection,
    info,
)
from django.conf import settings
from json import loads
from django.contrib.gis.geos import Point

# TILE_BASE = getattr(settings, 'GEOTHERMIE_TILE_BASE', 32)
MIN_ZOOM = getattr(settings, "GEOTHERMIE_MIN_ZOOM", 5)
MAX_ZOOM = getattr(settings, "GEOTHERMIE_MAX_ZOOM", 6)


def double(n, base):
    if n <= 0:
        return 0
    return base * pow(2, n - 1)


def zoom_from_z(z):
    f = 0.7 * 2 * 2
    if z < MIN_ZOOM:
        return floor(double(MIN_ZOOM, f))
    return floor(double(z, f))


TILE_MINX, TILE_MINY, TILE_MAXX, TILE_MAXY = [
    140000.000,
    160000.000,
    159000.000,
    179000.000,
]


def out_of_range(x, y):
    return x < TILE_MINX or x > TILE_MAXX or y < TILE_MINY or y > TILE_MAXY


def nodata(xs):
    for x in xs:
        if x > 0:
            return False
    return True


@cache_page(60 * 60 * 60)
def tile(request, base, minx, miny, maxx, maxy):

    """Return a tile of data of base x base
    with extent [minx, miny, maxx, maxy] (EPSG:31370)

    The whole data set is 1900x1900 points
    of 10x10 meters at origin south 140000, west 160000
    """

    # tile_size = 1900 * 10 // zoom
    width = maxx - minx
    height = maxy - miny
    max_side = width if width >= height else height
    tile_size = max_side // base
    data = []
    if tile_size <= 10:
        for row_y in range(miny, maxy, tile_size):
            row = []
            for row_x in range(minx, maxx, tile_size):
                if out_of_range(row_x, row_y):
                    row.append(
                        dict(
                            x=row_x,
                            y=row_y,
                            thickness=DEFAULT_POINT + [BASE_ROCK_DEPTH],
                            nodata=True,
                        )
                    )
                else:
                    thickness = get_point_thickness(row_x, row_y)
                    row.append(
                        dict(
                            x=row_x,
                            y=row_y,
                            thickness=thickness + [BASE_ROCK_DEPTH],
                            nodata=nodata(thickness),
                        )
                    )
            data.append(row)
    else:
        for row_y in range(miny, maxy, tile_size):
            row = []
            for row_x in range(minx, maxx, tile_size):
                if out_of_range(row_x, row_y):
                    row.append(
                        dict(
                            x=row_x,
                            y=row_y,
                            thickness=DEFAULT_POINT + [BASE_ROCK_DEPTH],
                            nodata=True,
                        )
                    )
                else:
                    thickness = get_mean_thickness(row_x, row_y, tile_size // 10)
                    row.append(
                        dict(
                            x=row_x,
                            y=row_y,
                            thickness=thickness + [BASE_ROCK_DEPTH],
                            nodata=nodata(thickness),
                        )
                    )
            data.append(row)

    data.reverse()
    return JsonResponse(dict(x=minx, y=miny, size=tile_size, data=data))


def raw_point(x, y):
    thickness = get_point_thickness(x, y)
    quat_thickness = get_point_quat_thickness(x, y)
    saturation_rate = get_point_saturation_rate(x, y)
    phreatic_head = get_point_phreatic_head(x, y)
    piezo = get_point_piezo(x, y)

    return dict(
        x=x,
        y=y,
        thickness=thickness,
        quat_thickness=quat_thickness,
        saturation_rate=saturation_rate,
        phreatic_head=phreatic_head,
        piezo=piezo,
        nodata=False,
    )


def mean_point(x, y, size):
    thickness = get_mean_thickness(x, y, size)
    quat_thickness = get_point_quat_thickness(x, y)
    saturation_rate = get_point_saturation_rate(x, y)
    phreatic_head = get_point_phreatic_head(x, y)
    piezo = get_point_piezo(x, y)

    return dict(
        x=x,
        y=y,
        thickness=thickness,
        quat_thickness=quat_thickness,
        saturation_rate=saturation_rate,
        phreatic_head=phreatic_head,
        piezo=piezo,
        nodata=False,
    )


def point(request, x, y):
    try:
        point_data = raw_point(x, y)
    except IndexError:
        return HttpResponseBadRequest("Coordinates are not in range")

    return JsonResponse(point_data)


def order_points(x0, y0, x1, y1):
    if x0 > x1:
        return x1, y1, x0, y0
    return x0, y0, x1, y1


def distance(c1, c2):
    """
    Returns the distance between two coordinates pairs as c1 = [x1, y1] and c2 = [x2, y2]
    """
    return sqrt((c2[0] - c1[0]) ** 2 + (c2[1] - c1[1]) ** 2)


def line_distance(coords):
    """
    Returns the total distance of a line expressed by its coordinates as
    coords = [[x1, y1], [x2, y2], ..., [xn, yn]]
    """
    dist = 0
    for i in range(len(coords) - 1):
        dist = dist + distance(coords[i + 1], coords[i])
    return dist


def point_dict(x, y, size):
    """
    Returns a point dict for computation along a line
    """
    x = round(x)
    y = round(y)
    if out_of_range(x, y):
        point = dict(x=x, y=y, thickness=DEFAULT_POINT + [BASE_ROCK_DEPTH], nodata=True)

    else:
        if size > 100:
            point = mean_point(
                x, y, round(size / 2 / 10)
            )  # size is expressed in 10 meters, and average is made by getting - 5 and +5 m on each direction
        else:
            point = raw_point(x, y)

    return point


@require_http_methods(["POST"])
def multi_points_line(request, base):
    """
    Multi-points line API:

    Returns an array of minimum *base* points sampled at equidistance along a line [..coords].
    The equidistance is computed for the whole line.

    Sampling is also performed at every points of the original line (i.e., in the *coords* array).
    """
    geom = GEOSGeometry(request.body.decode("utf-8"))
    if geom.geom_type != "LineString":
        raise Exception(f'unsupported geometry type: "{geom.geom_type}"')

    coords_arr = geom.coords
    line_dist = line_distance(coords_arr)
    step_size = round(line_dist / base)

    points = []

    for i in range(len(coords_arr) - 1):
        [segment_start_x, segment_start_y] = coords_arr[i]
        [segment_end_x, segment_end_y] = coords_arr[i + 1]
        segment_dist = distance(
            [segment_start_x, segment_start_y], [segment_end_x, segment_end_y]
        )

        delta_x = segment_end_x - segment_start_x
        delta_y = segment_end_y - segment_start_y

        j = 0
        pi = [segment_start_x, segment_start_y]

        while distance(pi, [segment_start_x, segment_start_y]) < segment_dist:

            # When j == 0, do sampling at the starting point of the segment
            points.append(point_dict(pi[0], pi[1], step_size))

            j = j + 1

            x = round(segment_start_x + j * delta_x * step_size / segment_dist)
            y = round(segment_start_y + j * delta_y * step_size / segment_dist)

            pi = [x, y]

    # Add the last point
    [x, y] = coords_arr[-1]
    points.append(point_dict(x, y, step_size))

    return JsonResponse(points, safe=False)


def line(request, base, x0, y0, x1, y1):
    """
    Line API:

    Returns an array of *base* points sampled at equidistance along a straight line [(x0, y0), (x1, y1)]
    """

    x0, y0, x1, y1 = order_points(x0, y0, x1, y1)
    delta_x = x1 - x0
    delta_y = y1 - y0
    dist = sqrt(delta_x**2 + delta_y**2)
    size = round(dist / base)

    points = []
    for step in range(base):
        x = round(x0 + step * delta_x / (base - 1))
        y = round(y0 + step * delta_y / (base - 1))
        points.append(point_dict(x, y, size))

    return JsonResponse(points, safe=False)


def constraint(request, x, y):
    try:
        natura = get_point_natura(x, y)
        water = get_point_water(x, y)
        soil = get_point_soil(x, y)
        region = get_point_region(x, y)
        metro_nord = get_point_metro_nord(x, y)
        cons_dict = dict(
            natura=natura, water=water, soil=soil, region=region, metro_nord=metro_nord
        )
    except IndexError:
        return HttpResponseBadRequest("Coordinates are not in range")

    return JsonResponse(dict(x=x, y=y, constraints=cons_dict))


def map_selection(request):
    selection = []
    for ms in MapSelection.objects.all():
        selection.append(
            dict(
                id=ms.id,
                map=ms.map.id,
                sort=ms.sort_index,
                panel=ms.panel,
            )
        )

    return JsonResponse(selection, safe=False)


class CapakeyRequester:
    class NoModelError(Exception):
        pass

    def __init__(self):
        from postgis_loader.models import get_layer

        try:
            capakey_settings = getattr(
                settings,
                "GEOTHERMIE_CAPAKEY",
                dict(
                    schema="base_plan",
                    table="urbis_cadastre",
                    capakey="capakey",
                ),
            )
            model, geometry_field, _geometry_field_type = get_layer(
                capakey_settings["schema"],
                capakey_settings["table"],
            )
            self.column = capakey_settings["capakey"]
            self.geometry_field = geometry_field
            self.model = model
        except Exception:
            self.model = None

    def get_from_key(self, capakey):
        if self.model is not None:
            args = {self.column: capakey}
            base = loads(
                serialize(
                    "geojson",
                    self.model.objects.filter(**args),
                    geometry_field=self.geometry_field,
                    srid=31370,
                )
            )

            # we need to add IDs to features
            for i, f in enumerate(base["features"]):
                f["id"] = i

            return base

        raise CapakeyRequester.NoModelError()

    def get_from_position(self, x, y):
        if self.model is not None:
            point = Point(x, y, srid=31370)
            kwargs = {"{}__contains".format(self.geometry_field): point}
            base = loads(
                serialize(
                    "geojson",
                    self.model.objects.filter(**kwargs),
                    geometry_field=self.geometry_field,
                    srid=31370,
                )
            )

            # we need to add IDs to features
            for i, f in enumerate(base["features"]):
                f["id"] = i

            return base

        raise CapakeyRequester.NoModelError()


capa_requester = CapakeyRequester()


def capakey_in(capakey):
    return capakey.replace("-", "/")


def get_from_capakey(request, capakey):
    try:
        return JsonResponse(
            capa_requester.get_from_key(capakey_in(capakey)), safe=False
        )
    except CapakeyRequester.NoModelError:
        return HttpResponseNotFound()


def get_from_position(request, x, y):
    try:
        return JsonResponse(capa_requester.get_from_position(x, y), safe=False)
    except CapakeyRequester.NoModelError:
        return HttpResponseNotFound()


INFO_MODELS = {
    "strati": info.GwBgtTableStrati,
    "open": info.GwBgtTableOpen,
    "hgeol": info.GwBgtTableHgeol,
    "closed": info.GwBgtTableClosed,
    "litho": info.GwBgtLithoColumn,
}


def get_info(request, key):
    if key not in INFO_MODELS:
        return HttpResponseNotFound("No such info: {}".format(key))

    django_data = loads(serialize("json", INFO_MODELS[key].objects.all()))
    return JsonResponse([o["fields"] for o in django_data], safe=False)


def get_info_bulles(request):
    data = []
    for bulle in info.InfoBulle.objects.all():
        data.append(
            dict(
                id=bulle.id,
                group=bulle.group.id,
                content=bulle.content.to_dict(),
            )
        )
    return JsonResponse(data, safe=False)
