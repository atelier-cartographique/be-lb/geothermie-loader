# Generated by Django 2.2.11 on 2020-06-16 13:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0022_auto_20191009_1403'),
        ('lingua', '0001_initial'),
        ('geothermie', '0001_initial'),
    ]

    operations = [
       
        migrations.CreateModel(
            name='bulle',
            fields=[
            ],
            options={
                # 'constraints': [], django 2.1.5 doesn'nt like it
                'indexes': [],
                'proxy': True,
            },
            bases=('lingua.messagerecord',),
        ),
        migrations.CreateModel(
            name='InfoBulle',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bulle', to='geothermie.bulle')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.LayerGroup')),
            ],
        ),
    ]
