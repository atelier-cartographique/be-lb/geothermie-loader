from django.contrib import admin

from .models import MapSelection, info


class MapSelectionAdmin(admin.ModelAdmin):
    list_display = ('map', 'panel', 'sort_index')

class InfoBulleAdmin(admin.ModelAdmin):
    list_display = ('group', 'content')


admin.site.register(MapSelection, MapSelectionAdmin)
admin.site.register(info.InfoBulle, InfoBulleAdmin)
