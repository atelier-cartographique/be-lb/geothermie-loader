from postgis_loader.models import get_layer
from django.contrib.gis.geos import Point
from django.conf import settings
from django.core.checks import Error, register
"""
This module is parameterized with a dict of the form:
GEOTHERMIE_CONTRAINTS_TABLES = {
    "name1" : ("schema1", "table1"),
    "name2" : ("schema2", "table2"),
    ...
}
ATM we expect `natura`, `water`, `soil` and `region`.
"""
CONSTRAINTS_TABLES = getattr(settings, 'GEOTHERMIE_CONTRAINTS_TABLES', None)


@register()
def check_contraints_configured(app_configs, **kwargs):
    errors = []
    if getattr(settings, 'GEOTHERMIE_CONTRAINTS_TABLES', None) is None:
        errors.append(
            Error(
                'Contraints Not Configured',
                hint='Set GEOTHERMIE_CONTRAINTS_TABLE in your settings',
                obj=settings,
                id='geothermie.E001',
            ))
    return errors


def constraint(name, default):
    def decorator(func):
        constraint_config = CONSTRAINTS_TABLES.get(
            name, None) if CONSTRAINTS_TABLES is not None else None

        def inner(x, y):
            if constraint_config is None:
                return default
            layer, geometry_field, _ = get_layer(*constraint_config)
            return func(x, y, layer, geometry_field)

        return inner

    return decorator


def make_GEOS_point(x, y):
    return Point(x, y, srid=31370)


def is_point_in_constraint(x, y, consLayer, geometry_field):
    pnt = make_GEOS_point(x, y)
    kwargs = {'{}__contains'.format(geometry_field): pnt}
    cond = consLayer.objects.filter(**kwargs)
    if cond.exists:
        return bool(cond)


def get_point_value_in_constraint(x, y, consLayer, geometry_field):
    pnt = make_GEOS_point(x, y)
    kwargs = {'{}__contains'.format(geometry_field): pnt}
    try:
        return str(
            consLayer.objects.filter(
                **kwargs).values('cat')[0]['cat'])
    except:
        print('error getting soil values')
        return str('none')


@constraint('natura', False)
def get_point_natura(x, y, naturaLayer, geometry_field):
    return is_point_in_constraint(x, y, naturaLayer, geometry_field)


@constraint('water', False)
def get_point_water(x, y, waterLayer, geometry_field):
    return is_point_in_constraint(x, y, waterLayer, geometry_field)


@constraint('soil', '-9999')  # why a string?
def get_point_soil(x, y, soilLayer, geometry_field):
    return get_point_value_in_constraint(x, y, soilLayer, geometry_field)


@constraint('region', 'none')
def get_point_region(x, y, regionLayer, geometry_field):
    return get_point_value_in_constraint(x, y, regionLayer, geometry_field)


@constraint('metro_nord', False)
def get_point_metro_nord(x, y, metroNordLayer, geometry_field):
    return is_point_in_constraint(x, y, metroNordLayer, geometry_field)