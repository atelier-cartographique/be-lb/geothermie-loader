# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models
from lingua.fields import nullable_text_field

def get_manager():
    return models.Manager().db_manager('groundwater')


# class GwHgeolStratiUs(models.Model):
#     gid = models.AutoField(primary_key=True)
#     ref_tstrati = models.ForeignKey('GwHgeolTstrati',
#                                     models.DO_NOTHING,
#                                     db_column='ref_tstrati',
#                                     blank=True,
#                                     null=True)
#     code_us = models.CharField(unique=True, max_length=80)
#     is_sus = models.BooleanField()
#     us_rbc = models.CharField(max_length=80, blank=True, null=True)
#     se_bhg = models.CharField(max_length=80, blank=True, null=True)
#     ref_uh = models.ForeignKey('GwHgeolUh',
#                                models.DO_NOTHING,
#                                db_column='ref_uh',
#                                blank=True,
#                                null=True)
#     ref_suh = models.ForeignKey('GwHgeolSuh',
#                                 models.DO_NOTHING,
#                                 db_column='ref_suh',
#                                 blank=True,
#                                 null=True)
#     ref_form_field = models.ForeignKey(
#         'GwStratiFormation',
#         models.DO_NOTHING,
#         db_column='ref_form_',
#         blank=True,
#         null=True)  # Field renamed because it ended with '_'.
#     ref_litho = models.ForeignKey('GwStratiLitho',
#                                   models.DO_NOTHING,
#                                   db_column='ref_litho',
#                                   blank=True,
#                                   null=True)
#     descrfr = models.TextField(blank=True, null=True)
#     descrnl = models.TextField(blank=True, null=True)
#     color = models.CharField(max_length=80, blank=True, null=True)

#     objects = get_manager()

#     class Meta:
#         managed = False
#         db_table = 'gw_hgeol_strati_us'

# class GwHgeolUh(models.Model):
#     gid = models.AutoField(primary_key=True)
#     ref_tgwref = models.ForeignKey('GwHgeolTgwref',
#                                    models.DO_NOTHING,
#                                    db_column='ref_tgwref',
#                                    blank=True,
#                                    null=True)
#     code_uh = models.CharField(unique=True,
#                                max_length=80,
#                                blank=True,
#                                null=True)
#     uh_rbc = models.CharField(max_length=80, blank=True, null=True)
#     he_bhg = models.CharField(max_length=80, blank=True, null=True)
#     ref_suh = models.ForeignKey('GwHgeolSuh',
#                                 models.DO_NOTHING,
#                                 db_column='ref_suh',
#                                 blank=True,
#                                 null=True)
#     ref_h_syst = models.ForeignKey('GwHgeolSystem',
#                                    models.DO_NOTHING,
#                                    db_column='ref_h_syst',
#                                    blank=True,
#                                    null=True)
#     ref_h_type = models.ForeignKey('GwHgeolType',
#                                    models.DO_NOTHING,
#                                    db_column='ref_h_type',
#                                    blank=True,
#                                    null=True)
#     ref_h_etat = models.ForeignKey('GwHgeolEtat',
#                                    models.DO_NOTHING,
#                                    db_column='ref_h_etat',
#                                    blank=True,
#                                    null=True)
#     uh_base = models.CharField(max_length=80, blank=True, null=True)

#     objects = get_manager()

#     class Meta:
#         managed = False
#         db_table = 'gw_hgeol_uh'


class GwBgtLithoColumn(models.Model):
    gid = models.IntegerField(primary_key=True)
    code_us = models.CharField(max_length=80, blank=True, null=True)
    ref_litho = models.IntegerField(blank=True, null=True)
    code_fgdc = models.IntegerField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_bgt_litho_column'


class GwBgtTableClosed(models.Model):
    gid = models.IntegerField(primary_key=True)
    code_us = models.CharField(max_length=80, blank=True, null=True)
    us_rbc = models.CharField(max_length=80, blank=True, null=True)
    se_bhg = models.CharField(max_length=80, blank=True, null=True)
    color = models.CharField(max_length=80, blank=True, null=True)
    lambda_dry = models.FloatField(blank=True, null=True)
    lambda_sat = models.FloatField(blank=True, null=True)
    remark_fr = models.TextField(blank=True, null=True)
    remark_nl = models.TextField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_bgt_table_closed'


class GwBgtTableHgeol(models.Model):
    gid = models.IntegerField(primary_key=True)
    code_uh = models.CharField(max_length=80, blank=True, null=True)
    uh_rbc = models.CharField(max_length=80, blank=True, null=True)
    he_bhg = models.CharField(max_length=80, blank=True, null=True)
    ref_h_type = models.IntegerField(blank=True, null=True)
    color = models.CharField(max_length=80, blank=True, null=True)
    ref_h_etat = models.IntegerField(blank=True, null=True)
    h_etat = models.CharField(max_length=80, blank=True, null=True)
    h_staat = models.CharField(max_length=80, blank=True, null=True)
    ref_h_syst = models.IntegerField(blank=True, null=True)
    h_syst_fr = models.CharField(max_length=80, blank=True, null=True)
    h_syst_nl = models.CharField(max_length=80, blank=True, null=True)
    code_geogr = models.CharField(max_length=80, blank=True, null=True)
    kmin = models.FloatField(blank=True, null=True)
    kavg = models.FloatField(blank=True, null=True)
    kmax = models.FloatField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_bgt_table_hgeol'


class GwBgtTableStrati(models.Model):
    gid = models.IntegerField(primary_key=True)
    code_us = models.CharField(max_length=80, blank=True, null=True)
    us_rbc = models.CharField(max_length=80, blank=True, null=True)
    se_bhg = models.CharField(max_length=80, blank=True, null=True)
    color = models.CharField(max_length=80, blank=True, null=True)
    descrfr = models.TextField(blank=True, null=True)
    descrnl = models.TextField(blank=True, null=True)
    ref_form_field = models.IntegerField(
        db_column='ref_form_', blank=True,
        null=True)  # Field renamed because it ended with '_'.
    ref_etage = models.IntegerField(blank=True, null=True)
    etage_fr = models.CharField(max_length=80, blank=True, null=True)
    etage_nl = models.CharField(max_length=80, blank=True, null=True)
    ref_serie = models.IntegerField(blank=True, null=True)
    serie_fr = models.CharField(max_length=80, blank=True, null=True)
    serie_nl = models.CharField(max_length=80, blank=True, null=True)
    ref_syst = models.IntegerField(blank=True, null=True)
    syst_fr = models.CharField(max_length=80, blank=True, null=True)
    syst_nl = models.CharField(max_length=80, blank=True, null=True)
    ref_ere = models.IntegerField(blank=True, null=True)
    ere_fr = models.CharField(max_length=80, blank=True, null=True)
    ere_nl = models.CharField(max_length=80, blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_bgt_table_strati'


class GwBgtTableOpen(models.Model):
    gid = models.IntegerField(primary_key=True)
    code_uh = models.CharField(max_length=80, blank=True, null=True)
    uh_rbc = models.CharField(max_length=80, blank=True, null=True)
    he_bhg = models.CharField(max_length=80, blank=True, null=True)
    ref_h_type = models.IntegerField(blank=True, null=True)
    color_uh = models.CharField(max_length=80, blank=True, null=True)
    ref_h_etat = models.IntegerField(blank=True, null=True)
    h_etat = models.CharField(max_length=80, blank=True, null=True)
    h_staat = models.CharField(max_length=80, blank=True, null=True)
    code_geogr = models.CharField(max_length=80, blank=True, null=True)
    kmin = models.FloatField(blank=True, null=True)
    kavg = models.FloatField(blank=True, null=True)
    kmax = models.FloatField(blank=True, null=True)
    poten_fr = models.CharField(max_length=80, blank=True, null=True)
    poten_nl = models.CharField(max_length=80, blank=True, null=True)
    rmk_pot_fr = models.TextField(blank=True, null=True)
    rmk_pot_nl = models.TextField(blank=True, null=True)
    color_pot = models.CharField(max_length=80, blank=True, null=True)
    selectable = models.BooleanField(blank=True, null=True)

    objects = get_manager()

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'gw_bgt_table_open'



class InfoBulle(models.Model):
    """Store a long description for a layer group
    """
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey('api.LayerGroup', on_delete=models.CASCADE)
    # content = text_field('bulle', app_name='geothermie')
    content = nullable_text_field('bulle')