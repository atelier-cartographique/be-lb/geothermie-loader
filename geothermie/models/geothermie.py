from django.db import models


class MapSelection(models.Model):
    TOP = 'top'
    BOTTOM = 'bot'
    PANEL_CHOICE = (
        (TOP, 'Top Panel'),
        (BOTTOM, 'Bottom Panel'),
    )
    id = models.Aggregate(primary_key=True)
    map = models.ForeignKey('api.UserMap', on_delete=models.CASCADE)
    sort_index = models.IntegerField(default=1)
    panel = models.CharField(max_length=3, choices=PANEL_CHOICE)
