from django.contrib.gis.gdal import GDALRaster
from django.conf import settings
import json
from pathlib import PosixPath
from collections import namedtuple
import numpy as np
from math import isnan, inf

SOURCE_DIR_SETTING = getattr(settings, 'GEOTHERMIE_SOURCE_DIR', None)
SOURCE_DIR = PosixPath(
    SOURCE_DIR_SETTING) if SOURCE_DIR_SETTING is not None else None

SOURCES = (
    'thickness/mnt_brustrati3dv1_1_masked_rbc.tif',
    'thickness/uhrbc1_quaternaire_epa.tif', 'thickness/usrbc21_diest_epa.tif',
    'thickness/usrbc22_bolderberg_epa.tif',
    'thickness/usrbc23_st_huibrechts_hern_epa.tif',
    'thickness/usrbc25_onderdale_epa.tif',
    'thickness/usrbc31_ursel_asse_epa.tif', 'thickness/usrbc41_wemmel_epa.tif',
    'thickness/usrbc42_lede_epa.tif', 'thickness/usrbc43_bruxelles_epa.tif',
    'thickness/usrbc44_vlierzele_epa.tif',
    'thickness/usrbc51_merelbeke_epa.tif', 'thickness/usrbc61_tielt_epa.tif',
    'thickness/usrbc71_aalbeke_epa.tif', 'thickness/usrbc72_moen_epa.tif',
    'thickness/usrbc73_st_maur_epa.tif',
    'thickness/usrbc81_grandglise_epa.tif',
    'thickness/usrbc82_lincent_epa.tif', 'thickness/usrbc91_cretace_epa.tif',
    'quat_thickness/usrbc11_12_remblais_limons_epa.tif',
    'quat_thickness/usrbc11_13_remblais_limons_argiles_epa.tif',
    'quat_thickness/usrbc14_graviers_alluviaux_epa.tif',
    'phreatic_head/phreatic_head_normtopolidar_rbc.tif',
    'piezo/uhrbc1b_quaternaire_limons_sables_graviers_alluviaux_bpsm.tif',
    'piezo/uhrbc2_systeme_perche_bpsm.tif', 'piezo/uhrbc4_bruxellien_bpsm.tif',
    'piezo/uhrbc6_tielt_bpsm.tif', 'piezo/uhrbc7b_moen_bpsm.tif',
    'piezo/uhrbc8a_landenien_hydroland.tif',
    'saturation_rate/usrbc11_12_remblais_limons_sat_rate.tif',
    'saturation_rate/usrbc11_13_remblais_limons_argiles_sat_rate.tif',
    'saturation_rate/usrbc14_graviers_alluviaux_sat_rate.tif',
    'saturation_rate/usrbc21_diest_sat_rate.tif',
    'saturation_rate/usrbc22_bolderberg_sat_rate.tif',
    'saturation_rate/usrbc23_st_huibrechts_hern_sat_rate.tif',
    'saturation_rate/usrbc25_onderdale_sat_rate.tif',
    'saturation_rate/usrbc31_ursel_asse_sat_rate.tif',
    'saturation_rate/usrbc41_wemmel_sat_rate.tif',
    'saturation_rate/usrbc42_lede_sat_rate.tif',
    'saturation_rate/usrbc43_bruxelles_sat_rate.tif',
    'saturation_rate/usrbc44_vlierzele_sat_rate.tif',
    'saturation_rate/usrbc51_merelbeke_sat_rate.tif',
    'saturation_rate/usrbc61_tielt_sat_rate.tif',
    'saturation_rate/usrbc71_aalbeke_sat_rate.tif',
    'saturation_rate/usrbc72_moen_sat_rate.tif',
    'saturation_rate/usrbc73_st_maur_sat_rate.tif',
    'saturation_rate/usrbc81_grandglise_sat_rate.tif',
    'saturation_rate/usrbc82_lincent_sat_rate.tif',
    'saturation_rate/usrbc91_cretace_sat_rate.tif')

STEP = 10.0


def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    # _yDist = geoMatrix[5]
    # _rtnX = geoMatrix[2]
    # _rtnY = geoMatrix[4]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


Accessor = namedtuple('Accessor', ['exact', 'mean'])


class NumericalHazard(Exception):
    pass


def make_accessor(path):
    ds = GDALRaster(path)
    transform = ds.geotransform
    data = ds.bands[0].data()
    ds = None

    def at_coords(lng, lat):
        x, y = world2Pixel(transform, lng, lat)
        v = float(data[y, x])
        if isnan(v):
            return 0.0
        return v

    def mean(lng, lat, size):
        x, y = world2Pixel(transform, lng, lat)
        v = float(np.mean(data[y:y + size, x:x + size]))

        # It's not really obvious to me how best we can handle these
        # numerical errors. If anybody gets a better idea, just push it.
        # But note that we cannot just let them go through as it breaks JSON.
        # - pm
        if isnan(v) or v == inf or v == -inf:
            # return 0.0
            raise NumericalHazard()

        return v

    return Accessor(at_coords, mean)


def filter_accessor(prefix):
    return [
        make_accessor(SOURCE_DIR.joinpath(fn).absolute().as_posix())
        for fn in SOURCES if fn.startswith(prefix)
    ] if SOURCE_DIR is not None else []


class Data:
    """
    We want to lazy load layers otherwise every interaction
    with Django becomes painfuly slow
    """
    acc_thickness = None  # filter_accessor('thickness')
    acc_quat_thickness = None  # filter_accessor('quat_thickness')
    acc_piezo = None  # filter_accessor('piezo')
    acc_saturation_rate = None  # filter_accessor('saturation_rate')
    acc_phreatic_head = None  # filter_accessor('phreatic_head')

    @staticmethod
    def thickness():
        if Data.acc_thickness is None:
            Data.acc_thickness = filter_accessor('thickness')
        return Data.acc_thickness

    @staticmethod
    def quat_thickness():
        if Data.acc_quat_thickness is None:
            Data.acc_quat_thickness = filter_accessor('quat_thickness')
        return Data.acc_quat_thickness

    @staticmethod
    def piezo():
        if Data.acc_piezo is None:
            Data.acc_piezo = filter_accessor('piezo')
        return Data.acc_piezo

    @staticmethod
    def saturation_rate():
        if Data.acc_saturation_rate is None:
            Data.acc_saturation_rate = filter_accessor('saturation_rate')
        return Data.acc_saturation_rate

    @staticmethod
    def phreatic_head():
        if Data.acc_phreatic_head is None:
            Data.acc_phreatic_head = filter_accessor('phreatic_head')
        return Data.acc_phreatic_head


def get_point_thickness(lng, lat):
    return [get.exact(lng, lat) for get in Data.thickness()]


def get_point_quat_thickness(lng, lat):
    return [get.exact(lng, lat) for get in Data.quat_thickness()]


def get_point_saturation_rate(lng, lat):
    return [get.exact(lng, lat) for get in Data.saturation_rate()]


def get_point_phreatic_head(lng, lat):
    return [get.exact(lng, lat) for get in Data.phreatic_head()]


def get_point_piezo(lng, lat):
    return [get.exact(lng, lat) for get in Data.piezo()]


def get_mean_thickness(lng, lat, size):
    try:
        return [get.mean(lng, lat, size) for get in Data.thickness()]
    except NumericalHazard:
        return DEFAULT_POINT


DEFAULT_POINT = [0] * 13
BASE_ROCK_DEPTH = 1000

if __name__ == "__main__":
    import sys
    lng_orig = float(sys.argv[1])
    lat_orig = float(sys.argv[2])

    result = []
    for lat in [lat_orig + n * STEP for n in range(60)]:
        band = []
        for lng in [lng_orig + n * STEP for n in range(60)]:
            band.append(get_point_thickness(lng, lat))
        result.append(band)

    print(json.dumps(result))
