from django.conf import settings
from django.core.checks import Error, register


@register()
def check_geothermie_config_source(app_configs, **kwargs):
    errors = []
    if getattr(settings, 'GEOTHERMIE_SOURCE_DIR', None) is None:
        errors.append(
            Error(
                'Missing Config For geothermie [GEOTHERMIE_SOURCE_DIR]',
                hint=
                'Set GEOTHERMIE_SOURCE_DIR in your settings to let the module know where it can find all the GeoTIFs it needs',
                obj=settings,
                id='geothermie.E001',
            ))
    return errors
